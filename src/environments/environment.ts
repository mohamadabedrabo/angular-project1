// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDVyScgowX-kq1Wsgi9stLbO5JAfNUwLnc",
    authDomain: "mohamad-exam.firebaseapp.com",
    projectId: "mohamad-exam",
    storageBucket: "mohamad-exam.appspot.com",
    messagingSenderId: "1031174923400",
    appId: "1:1031174923400:web:e750e075a0956d61596b3d"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
