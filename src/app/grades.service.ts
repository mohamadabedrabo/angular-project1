import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreCollectionGroup} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class GradesService {

  gradesCollection:AngularFirestoreCollection = this.db.collection('grades');

  result!:number;

  private url = "https://ky8qujknwl.execute-api.us-east-1.amazonaws.com/beta";

  getGrades(){
    this.gradesCollection = this.db.collection('grades');
    return this.gradesCollection.snapshotChanges().pipe(map(
          collection =>collection.map(
            document => {
              const data = document.payload.doc.data(); 
              data.id = document.payload.doc.id;
             return data; 
            }
          )
        ))      
  } 

  addGrades(math:number,psycho:number,payed:boolean) {
    const grades = {math:math, psycho:psycho,payed:payed};
    this.gradesCollection.add(grades);
  }

  predict(math:number,psycho:number,payed:boolean):Observable<any> {

    let json = {
      "data": 
      {
        "math": math,
        "psycho": psycho,
        "payed": payed
      }
    }

    let body = JSON.stringify(json);

    return this.http.post<any>(this.url, body).pipe(
      map(
        res => {
          console.log(res);
          let final:string = res.body;
          console.log(res);
          console.log(res.body);
          this.result=res.body;
          return res.body;
        }
      )
    )
  }

  constructor(private http:HttpClient,
              private db:AngularFirestore) { }
}
