export interface User {
    uid:string,
    email?:string | null,
    photoUrl?:string,
}
