import { GradesService } from './../grades.service';
import { Grades } from './../interfaces/grades';
import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'grades-form',
  templateUrl: './grades-form.component.html',
  styleUrls: ['./grades-form.component.css']
})

export class GradesFormComponent implements OnInit {

  result!:number;
  con!:string;

  predictButtonOpen = true;

  @Input() id!:string;
  @Input() math:number=0;
  @Input() psycho:number=0;
  @Input() payed:boolean=false;

  @Output() update = new EventEmitter<Grades>();

  cancel() {
    this.math = 0;
    this.psycho=0;
    this.result=0;
    this.con='';
  }




  predict() {
    this.gradesService.predict(this.math,this.psycho,this.payed).subscribe(
      resu => {
        console.log(resu);
        this.result = this.gradesService.result;
        if (this.result >= 0.5) {
          this.con = "Gonna quit";
        }
    
        if (this.result <= 0.5) {
          this.con = "Not Gonna quit";
        }  
      }
    )
  }

  updateParent() {
    let grades:Grades = {math:this.math,psycho:this.psycho,payed:this.payed};
    this.update.emit(grades);
    
  }



  constructor(private route:ActivatedRoute, 
              private gradesService:GradesService) { }

  ngOnInit(): void {
  }

}
