import { GradesService } from './../grades.service';
import { Grades } from './../interfaces/grades';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'grades',
  templateUrl: './grades.component.html',
  styleUrls: ['./grades.component.css']
})
export class GradesComponent implements OnInit {

  math!:number;
  psycho!:number;
  payed!:boolean;
  grades!:Grades;

  gradesFormOPen:boolean = false;


  add(grades:Grades){
    this.gradesService.addGrades(grades.math,grades.psycho,grades.payed); 
  }

  constructor(public authService:AuthService,
              private route:ActivatedRoute,
              private gradesService:GradesService) { }

  ngOnInit(): void {
  }

}
