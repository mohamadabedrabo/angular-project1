import { GradesService } from './../grades.service';
import { Grades } from './../interfaces/grades';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  grades$:any; 

  constructor(public authService:AuthService,
              private route:ActivatedRoute,
              private gradesService:GradesService) { }

  ngOnInit(): void {
    this.grades$ = this.gradesService.getGrades();
  }

}
